# 1. Pure Bruteforce [_accepted_]

```java
    //O(n^2)
    public boolean bruteForceFirstTry(int[] nums, int k) {
        for(int i = 0; i < nums.length - 1; i++)
        {
            for(int j = i + 1; j < nums.length; j++)
            {
                if( (nums[i] == nums[j]) && (j-i <= k) )
                    return true;
            } 
        }
        
        return false;
    }
```

# 2. Using set

```java
    public boolean setFirstTry(int[] nums, int k) {
      
        //Map<Number, recentIndex>
        Map<Integer, Integer> map = new HashMap<>();
                
        for(int i = 0; i < nums.length; i++)
        {
            if(! map.containsKey(nums[i]))
                map.put(nums[i], i);
            else
            {
                if( ( i - map.get(nums[i]) ) <= k)
                {
                    //System.out.println(nums[i] +"\t" + map.get(nums[i]) + "\t" + i+"\t"+(map.get(nums[i]) - i));
                    return true;                    
                }
                else
                    map.put(nums[i], i);
            }
        }
        return false;
        
    }
    
```

## 2.1 Improved (reduced redundant code)
    //slight tuning of setFirstTry
    public boolean tuneSetFirstTry(int[] nums, int k) {
        
        Map<Integer, Integer> map = new HashMap<>();
        
        for(int i = 0; i < nums.length; i++)
        {
            if(map.containsKey(nums[i]) && ( i - (map.get(nums[i])) <= k) )
                return true;
            map.put(nums[i], i);
        }
        return false;
    }
    
# 3. My Favourite

    public boolean mostEfficientSln(int[] nums, int k)
    {
        Set<Integer> currentSlide = new HashSet<>();
        
        for(int i = 0; i < nums.length; i++)
        {
            if(!currentSlide.add(nums[i]))
                return true;
            if(currentSlide.size() == k+1)
                currentSlide.remove(nums[i-k]);
        }
        return false;
    }