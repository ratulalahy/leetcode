## First Try:

```java

class Solution {
    public List<List<String>> groupStrings(String[] strings) {
        return firstTry(strings);
    }
    
    public List<List<String>> firstTry(String[] strings) {
        Map<String, List> map = new HashMap<>();
        
        for(int i = 0; i < strings.length ; i++)
        {
            String key = getKey(strings[i]);
            
            if(!map.containsKey(key))
                map.put(key, new ArrayList<>());
            map.get(key).add(strings[i]);
        }
        
        return new ArrayList(map.values());
    }
    
    public String getKey(String str)
    {
        //System.out.println("analyzing: "+str);
        char[] tempAry = str.toCharArray();
        int offset = tempAry[0] - 'a';
        
        //System.out.println("offset: "+offset);
        
        for(int i = 0; i< tempAry.length; i++)
        {
            //System.out.println(tempAry[i] - offset);
            
            int tempChar = tempAry[i] - offset;
            
            tempAry[i] = (char)( tempChar >= 97 ? tempChar : (tempChar + 26) ); 
        }
        //System.out.println("becomes: "+new String(tempAry));
        
        return new String(tempAry);
    }
}

```